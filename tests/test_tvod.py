import tvod


def test_version():
    assert tvod.__version__ == '2.0.1'

def test_sanitize_video_name():
    assert tvod._sanitize_video_name("", "123456") == "123456"
